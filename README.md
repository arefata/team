# EP Team project

## [Runbooks](runbooks/index.md)

## [Monitoring](monitoring.md)

## [Learning](learning.md)

## [Local tooling setup](local_setup.md)

## [Personal Productivity tips](personal_productivity_tips.md)
