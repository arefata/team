# Monitoring in the EP team

[[_TOC_]]

## Introduction

We use [Google Cloud Monitoring](https://cloud.google.com/monitoring) to be alerted of problems in our infrastructure.

Because we like [Infrastructure as Code](https://en.wikipedia.org/wiki/Infrastructure_as_code), we configure the monitoring via Terraform in the [engineering-productivity-infrastructure project](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/).

## Concepts

(This is a quick summary of the resources in [our learning documentation](learning.md))

### What can we alert on?

You can create [alerts based on metrics](https://cloud.google.com/monitoring/alerts/using-alerting-ui) (the usual), or [based on logs](https://cloud.google.com/logging/docs/alerting/monitoring-logs). Both have plenty of use-cases.

### What is an alerting policy?

Describes **when an alert should be created** (e.g. pods are crashing every minute, triage-ops has been down for 5 minutes), and **how you should be notified about alerts** (e.g. Slack, email, ...).

You can find examples of those alerting policies in [our infrastructure project](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/triage-reactive/alerts.tf).

### What is a notification channel?

Basically, how to contact humans when an alert fires (e.g. via email, slack, ...).

**Terraform implementation**

We create notification channels **manually in the UI, not via Terraform**. The reason why is explained in https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/merge_requests/88#about-slack-as-a-notification-channel-in-terraform.

Also note that `terraform import` for a notification channel never worked for us so far, so we instead [pass the notification channels URLs as variables to Terraform](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/triage-reactive/variables.tf#L1-9). Those Terraform variables are [directly passed to `google_monitoring_alert_policy` resources](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/triage-reactive/alerts.tf#L38-40).

## Add a new resource to the monitoring

* In [this MR](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/merge_requests/88), we added an uptime check for triage-ops application on Kubernetes.

## Why don't we use other solutions like Prometheus?

A crude argumentation is given in https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/issues/22#note_1134594310.
