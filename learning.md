# Learning resources

[[_TOC_]]

## Kubernetes

If you feel like your Kubernetes knowledge could be improved, this is the place to start!

This section will start with generic knowledge, and specific Engineering Productivity know-how for Kubernetes.

Please document resources in here that helped you in your Kubernetes journey: it will benefit the entire team!

### Generic knowledge

* https://kubernetes.io/docs/tutorials/kubernetes-basics/ - interactive tutorial to start with practical know-how
* https://kubernetes.io/docs/concepts/overview/ - follow the next links from there
* Prepare for a [CKAD certification](https://www.cncf.io/certification/ckad/) or a [CKA certification](https://www.cncf.io/certification/cka/) (e.g. follow a class on Udemy, buy a book, whatever works best for you).
* Ask questions to teammates (Engineering Productivity, #kubernetes Slack channel), which is an invaluable resource when you are stuck on a concept and/or command!

### Engineering Productivity "generic" knowledge

* Learn about [GKE](https://cloud.google.com/kubernetes-engine/docs/concepts/kubernetes-engine-overview), and explore their [what's next](https://cloud.google.com/kubernetes-engine/docs/concepts/kubernetes-engine-overview#whats_next) section.
* [Setup your local environment](./local_setup.md)
* Learn about [a few useful gcloud commands](https://cloud.google.com/sdk/docs/cheatsheet#docker)
* [Read about `helm`](https://helm.sh/docs/)

### Engineering Productivity "specific" knowledge

* If not already done above, [setup your local environment](./local_setup.md)
* Have a look at our GKE clusters in the GCP console. How many clusters do we have in total?
* Understand how we deploy `triage-ops` to Kubernetes
* Research on how we deploy `review-apps` (hint: just look at the `review-deploy` CI job)
* Read on [how to upgrade certain Helm charts we depend on](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/UPGRADE.review-apps.md)
* Upgrade some charts yourself!

## Monitoring

### GCP resources

* https://cloud.google.com/monitoring/alerts
* https://cloud.google.com/monitoring/support/notification-options
* https://cloud.google.com/monitoring/uptime-checks

### Terraform resources

* https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/monitoring_alert_policy
* https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/monitoring_notification_channel
* https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/monitoring_uptime_check_config
