# RUNBOOK - Engineering Productivity Infrastructure

This RUNBOOK is for production issues in the https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure project.

[[_TOC_]]

## Production Incidents

### googleapi: Error 403: Permission denied (or the resource may not exist).

#### The problem

Terraform, when doing a `terraform apply`, cannot seem to create some resources.

To makes things worse, everything was working properly in your GCP sandbox when you tested it out!

#### How to diagnose

* See the output of the `terraform apply` job.

#### How to fix it

* This is most likely that the Terraform GCP service account doesn't have the IAM rights to create the resource, because it doesn't have the correct role.
  * Go to [the GCP IAM console](https://console.cloud.google.com/iam-admin/iam)
  * Pick the GCP project you'd like to work from (e.g. `gitlab-review-apps`, `gitlab-qa-resources`, ...)
  * In the filter, search for `engineering-productivity-infra`. This is the name of the [GCP Service Account (SA)](https://cloud.google.com/iam/docs/service-accounts) that Terraform uses to deploy resources in GCP.
  * Check the [GCP roles](https://cloud.google.com/iam/docs/understanding-roles) that the GCP SA has assigned.
  * Check with the team whether a GCP Role should be added to this GCP SA.
  * Add the minimal roles necessary to the GCP SA manually.
  * Trigger again the `terraform plan` and `terraform apply` jobs.

### googleapi: Error 400: Request contains an invalid argument.

#### The problem

Terraform, when doing a `terraform apply`, cannot seem to create some resources.

#### How to diagnose

* See the output of the `terraform apply` job.

#### How to fix it

* If applicable, remove the GCP resource manually
* Trigger a new pipeline

## Learning

### You have received an MR to review for an upgrade. What now?

Have a look at [the upgrade documentation in the project](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/UPGRADE.review-apps.md).
