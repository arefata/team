# RUNBOOK - Predictive Tests

This technical documentation explains what mechanisms are in place to NOT run the entire RSpec/Jest test suites in `gitlab-org/gitlab` CI/CD pipelines. Its purpose is to provide a blueprint to the people wanting to maintain/fix/evolve this part of the CI/CD architecture.

If you are unfamiliar with predictive tests, please start with [What are predictive tests?](#what-are-predictive-tests).

[[_TOC_]]

## Production Incidents

### You found a test selection gap, now what?

#### The problem

You have a report of a test that wasn't run as part of an MR, and it should have been.

#### How to diagnose

* There is a broken master incident for which the root cause analysis was a test selection gap (i.e. the ~"master-broken::test-selection-gap" is applied to the incident)
* You noticed it by looking at MR pipelines
* Somebody told you about it

#### How to fix it

It could be that the test wasn't run because of a missing CI/CD rule. In that case, changing CI/CD rules to run particular jobs in case certain files change is the way to go (see [an example](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114452)).

If the correct jobs were run, but the tests were not selected by [the predictive tests process](#what-are-predictive-tests):

* Evaluate whether the tests could be included with a `static mapping`
* If not, consider whether we could extend `existing dynamic mappings`
* If it's not possible to easily extend existing dynamic mappings, consider writing a new dynamic mapping for including those tests.

## Learning

### What are predictive tests?

Have a look at [the GitLab pipelines development documentation](https://docs.gitlab.com/ee/development/pipelines/index.html#predictive-test-jobs-before-a-merge-request-is-approved).

### How to diagnose a potential predictive tests problem?

* Open the `detect-tests` job in the pipeline
  * See which tests were selected by looking at the output
  * Optionally, download the artifacts, as it contains all the files the `detect-tests` wrote for subsequent jobs to use.
* If you suspect an issue with dynamic mappings, run the different mappings locally, and see the output of each of them. Below is an example to test the `js_to_system_specs_mappings` dynamic mapping:

```shell
export RSPEC_CHANGED_FILES_PATH=rspec/input.in
export RSPEC_MATCHING_TESTS_PATH=rspec/output.out
export RSPEC_VIEWS_INCLUDING_PARTIALS_PATH=rspec/views_including_partials.txt
export FRONTEND_FIXTURES_MAPPING_PATH=XXX
export RSPEC_MATCHING_JS_FILES_PATH=rspec/js_matching_files.txt

mkdir rspec

# Add the files that were changed in the MR
cat > "${RSPEC_CHANGED_FILES_PATH}" <<FILE
app/assets/javascripts/import/details/components/import_details_app.vue
app/assets/javascripts/import/details/components/import_details_table.vue
app/assets/javascripts/import/constants.js
app/assets/javascripts/import_entities/components/import_status.vue
locale/gitlab.pot
spec/frontend/import/details/components/import_details_table_spec.js
FILE

# TODO: To test only the dynamic mappings you think are at fault, comment out every line you don't need in `tooling/lib/tooling/predictive_tests.rb`.

tooling/bin/predictive_tests

# Check the tests that were selected
cat "${RSPEC_MATCHING_TESTS_PATH}"

# Cleaning up
rm -rf rspec
```

* Fix the issue, write specs for it, …

### How can we find more test selection gaps?

There are at least two ways to go about this:

#### Triage broken master incidents

The process involves identifying broken master incidents that are due to a test selection gap, finding out the root cause of the issue, and fixing the issue.

A list of distinct test selection gaps can be found [in this Sisense widget](https://app.periscopedata.com/app/gitlab/1116767/Test-Intelligence-Accuracy?widget=16457702&udv=0), and here is a [non-distinct list of test selection gaps](https://gitlab.com/gitlab-org/quality/engineering-productivity/master-broken-incidents/-/issues/?sort=created_date&state=closed&label_name%5B%5D=master-broken%3A%3Atest-selection-gap&first_page_size=100).

#### Inspect existing MR pipelines

Here is [an investigation done for frontend tests](https://gitlab.com/gitlab-org/gitlab/-/issues/382740), and here is [an investigation done for backend tests](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/179).

[This Sisense dashboard](https://app.periscopedata.com/app/gitlab/1116767/Test-Intelligence-Accuracy) contains tables of predictive/full pipelines that respectively succeeded/failed, and those were the basis for the investigations mentioned earlier.

The main problem with those tables currently is that they are influenced by failed pipelines due to infrastructure issues and/or flaky tests. A code snippet was made as a first crude attempt to categorize failed pipelines ([$2507197](https://gitlab.com/gitlab-org/quality/engineering-productivity/master-broken-incidents/-/snippets/2507197)).

Once we have a reliable way to categorize pipeline failures, we'll be able to fully rely on those tables, and **compute an accurate predictive test accuracy metric**.
