# RUNBOOK - GDK

[[_TOC_]]

## Code review guidelines

### Review new service

Follow these guidelines when reviewing code changes that are related to introducing a new service:

- Consider whether the new service needs to be enabled by default. Some services in GDK are enabled by default, while others are optional and specific to certain teams or use cases.
- Encourage the MR author to include steps for verifying the proposed changes in the MR description.
- (Optional) Test the new service in your local GDK.
  - For example, you can run `make update` instead of `gdk update` to test the changes in the branch. This is because `make update` keeps the current branch while `gdk update` switches to the `main` branch.
- (Optional) You can ask team members who are more familiar with the domain of the new service for their feedback. This helps to ensure that the service will work as intended in the expected use cases.

### Review software version upgrades

Follow these guidelines when reviewing software version upgrades:

- When GDK's sub-projects (like GitLab, Gitaly, and so on) upgrade software versions, GDK also needs to update the `.tool-versions` file. But note that the `.tool-versions-gdk` file is different, as this file is specifically used to declare dependencies for GDK itself.
