# RUNBOOK - Rotating credentials

[[_TOC_]]

## Rotate a `@gitlab-bot` personal access token

Assuming you need to rotate a `@gitlab-bot` personal access token named `Token used for action X`, that is stored in the `GITLAB_BOT_ACCESS_TOKEN` variable of the project https://gitlab.com/example-group/example-project, here are the steps to rotate the token properly:

1. [As yourself] Check the current token stored in the GITLAB_BOT_ACCESS_TOKEN variable at
   https://gitlab.com/example-group/example-project/-/settings/ci_cd, with a request to
   `curl -H 'PRIVATE-TOKEN: <token>' https://gitlab.com/api/v4/user`. This should return the `@gitlab-bot` user data.
1. [As `@gitlab-bot`] In an incognito window, log into the `@gitlab-bot` account (credentials are in the Engineering 1Password vault).
1. [As `@gitlab-bot`] Look for the `Token used for action X` token at https://gitlab.com/-/profile/personal_access_tokens.
1. [As `@gitlab-bot`] Create a new personal access token with the same attributes as the `Token used for action X` one (e.g. scope: `api`, no expiration).
   For the sake of clarity, include the name of the project and environment variable where the token is stored in the
   new token name, e.g. `Token used for action X in example-group/example-project as GITLAB_BOT_ACCESS_TOKEN`.
1. [As yourself] Update the `GITLAB_BOT_ACCESS_TOKEN` variable value with the new access token at https://gitlab.com/example-group/example-project/-/settings/ci_cd.
1. [As `@gitlab-bot`] Revoke the `Token used for action X` token at https://gitlab.com/-/profile/personal_access_tokens.
1. [As yourself] Check the revoked token again, with a request to `curl -H 'PRIVATE-TOKEN: <token>' https://gitlab.com/api/v4/user`.
   This should return a "token revoked" error response:

    ```
    {
       "error": "invalid_token",
      "error_description": "Token was revoked. You have to re-authorize from the user."
    }
    ```

1. [As `@gitlab-bot`] Log-out of the `@gitlab-bot` account and close the incognito window.
1. Be prepared to be contacted on Slack by a SIRT team member asking to confirm if you logged-in as `@gitlab-bot`.
   Reply to them that, "yes it was you", and give a link to the context (i.e. an issue, or a Slack message).

## Variables rotation for https://gitlab.com/gitlab-org/gitlab

These are defined in https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd.

### `ADD_JH_FILES_TOKEN`

This is a [GitLab JH mirror](https://gitlab.com/gitlab-org/gitlab-jh-mirrors/gitlab)
project token with `read_api` permission, to be able to download JiHu files.

1. Create a new `ADD_JH_FILES_TOKEN` token with `read_api` scope, `Reporter` role and no expiration date at https://gitlab.com/gitlab-org/gitlab-jh-mirrors/gitlab/-/settings/access_tokens
1. Update the `ADD_JH_FILES_TOKEN` variable at
   - https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd
   - https://gitlab.com/gitlab-org/security/gitlab/-/settings/ci_cd
1. Revoke the previous token

### `AS_IF_JH_TOKEN`

This is a [GitLab JH validation](https://gitlab.com/gitlab-org-sandbox/gitlab-jh-validation)
project token with `write_repository` permission, to push generated `as-if-jh/*` branch.

1. Create a new `AS_IF_JH_TOKEN` token with `write_repository` scope, `Developer` role and no expiration date at https://gitlab.com/gitlab-org-sandbox/gitlab-jh-validation/-/settings/access_tokens
1. Update the `AS_IF_JH_TOKEN` variable at
   - https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd
   - https://gitlab.com/gitlab-org/security/gitlab/-/settings/ci_cd
1. Revoke the previous token

### `AWS_SIEM_REPORT_INGESTION_CREDENTIALS_FILE`

Ask someone from [the Security Logging team](https://about.gitlab.com/handbook/security/security-engineering/security-logging).

### `CI_SLACK_WEBHOOK_URL`

Request [a new Slack Incoming Webhook configuration](https://gitlab.slack.com/apps/A0F7XDUAZ-incoming-webhooks?tab=settings&next_id=0).

As not ideal as it sounds, this webhook URL is also currently used in:

- https://gitlab.com/gitlab-org/security/gitlab/-/settings/ci_cd
- https://gitlab.com/groups/gitlab-org/quality/-/settings/ci_cd
- https://ops.gitlab.net/groups/gitlab-org/quality/-/settings/ci_cd

### `CUSTOM_SAST_RULES_BOT_PAT`

1. Create a new `CUSTOM_SAST_RULES_BOT_PAT` token with `api` scope, `Guest` role and no expiration date at https://gitlab.com/gitlab-com/gl-security/appsec/sast-custom-rules/-/settings/access_tokens
1. Update the `CUSTOM_SAST_RULES_BOT_PAT` variable at https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd
1. Revoke the previous token

Ask [the Application Security engineer on rotation during the week](https://about.gitlab.com/handbook/security/security-engineering/application-security/runbooks/triage-rotation.html) for more information.

### `DOCKERHUB_USERNAME`, `DOCKERHUB_PASSWORD`

Ask [a Distribution Build engineer](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/#distribution-build-team).

### `DOCS_PROJECT_API_TOKEN`, `DOCS_TRIGGER_TOKEN`

Follow the steps at https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/doc/maintenance.md#regenerate-tokens (you'll have to update the variable value in multiple projects)
and ensure [the Technical Writer responsible for the monthly Docs maintenance task](https://about.gitlab.com/handbook/product/ux/technical-writing/#regularly-scheduled-tasks) is aware.

### `FAILING_ISSUES_PROJECT_TOKEN`

1. Create a new `FAILING_ISSUES_PROJECT_TOKEN` token with `api` scope, `Reporter` role and no expiration date at https://gitlab.com/gitlab-org/quality/engineering-productivity/flaky-tests-playground/-/settings/access_tokens
1. Update the `FAILING_ISSUES_PROJECT_TOKEN` variable at https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd
1. Revoke the previous token

### `GENERATE_TEST_SESSION_READ_API_REPORTER_TOKEN`

This is used in the `generate-test-session` job to retrieve jobs from the current pipeline.

It should be a project token set for each project that use this job (staging, canary, production etc.).

1. Create a new `GENERATE_TEST_SESSION_READ_API_REPORTER_TOKEN` token with `read_api` scope, `Reporter` and no expiration date at https://gitlab.com/<project>/-/settings/access_tokens
1. Update the `GENERATE_TEST_SESSION_READ_API_REPORTER_TOKEN` variable at https://gitlab.com/<project>/-/settings/ci_cd
1. Revoke the previous token

### `GITHUB_ACCESS_TOKEN`

**This is probably unused as we set `GITHUB_ACCESS_TOKEN: $QA_GITHUB_ACCESS_TOKEN` in end-to-end jobs.**

### `GITLAB_CI_API_TOKEN` (only set in https://ops.gitlab.net/groups/gitlab-org/quality/-/settings/ci_cd) - Deprecated, temporarily renamed to `UNUSED_GITLAB_CI_API_TOKEN`

**This shouldn't be used anymore, replaced by `GENERATE_TEST_SESSION_READ_API_REPORTER_TOKEN` (individual project tokens).**

This is currently a personal token of Jen-Shin on `ops.gitlab.net`! It's used in the `generate-test-session` job to retrieve jobs from the current pipeline.

### `GITLAB_PROJECT_PACKAGES_CLEANUP_API_TOKEN`

1. Create a new `GITLAB_PROJECT_PACKAGES_CLEANUP_API_TOKEN` token with `api` scope, `Maintainer` role (TODO: check if required) and no expiration date at https://gitlab.com/gitlab-org/gitlab/-/settings/access_tokens
1. Update the `GITLAB_PROJECT_PACKAGES_CLEANUP_API_TOKEN` variable at https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd
1. Revoke the previous token

### `GITLAB_PROJECT_REVIEW_APP_CLEANUP_API_TOKEN`

1. Create a new `GITLAB_PROJECT_REVIEW_APP_CLEANUP_API_TOKEN` token with `api` scope, `Maintainer` role (TODO: check if required) and no expiration date at https://gitlab.com/gitlab-org/gitlab/-/settings/access_tokens
1. Update the `GITLAB_PROJECT_REVIEW_APP_CLEANUP_API_TOKEN` variable at https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd
1. Revoke the previous token

### `GITLAB_QA_USER_AGENT`

The token is also stored in the `Production and Staging User Agent` 1Password item.

The `GITLAB_QA_USER_AGENT` environment variable is used for bypassing multiple restrictions including [ArkoseLabs](https://docs.gitlab.com/ee/integration/arkose.html#allowlists), [Cloud Flare Web Application Firewall (WAF)](https://gitlab.com/gitlab-com/gl-infra/cloudflare-firewall/-/issues/62) and [reCAPTCHA](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98045)

**NOTE:** 
  * This change is highly disruptive. All the actions should be performed simultaneously to minimise disruption to the deployment process, 
  * The steps should be performed in coordination with the Quality team's [pipeline triage DRI](https://gitlab.com/gitlab-org/quality/pipeline-triage#dri-weekly-rotation-schedule). Please reach out on the `#quality` Slack channel.

The user agent string is deployed into our GPRD, GSTG, and OPS environments by way of:
  1. Updating the `allowlist.yml` in the [cf_allowlist terraform module](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules)
  2. Once a new version of the module is made, the OPS, GSTG, and GPRD instances of this module should be updated to that version
     in the [Config Management](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt) project
  3. Examine/search the Chef Repo for instances of the string to be rotated, like [this instance](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/blob/master/roles/gstg-base-lb-fe-config.json)
  4. The [Hashicorp Vault](https://vault.gitlab.net) will also contain this secret. Known paths to these:
  - `k8s/show/env/gprd/ns/gitlab/gitlab-qa`
  - `k8s/show/env/gstg/ns/gitlab/gitlab-qa`

Once the above changes have been performed: 

  1. Update the `GITLAB_QA_USER_AGENT` CI/CD variable value in https://ops.gitlab.net/groups/gitlab-org/quality/-/settings/ci_cd. If you do not have access to perform this action, please reach out on the`#quality` Slack channel.
  1. Ask anyone in the #g_anti-abuse Slack channel to ask ArkoseLabs to update the value on their side (whitelist definitions) for Staging and Production.  

If you do not have access to these repositories and vault, contact the [Reliability Team](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/#general-workflow) to get assistance making these changes.

### `GITLABCOM_DATABASE_TESTING_TRIGGER_TOKEN`

This is a trigger token from the ops project at https://ops.gitlab.net/gitlab-com/database-team/gitlab-com-database-testing.

Ask [a Database engineer](https://about.gitlab.com/handbook/product/categories/#database-group).

The database engineer should take the following steps:

1. Go to https://ops.gitlab.net/gitlab-com/database-team/gitlab-com-database-testing/-/settings/ci_cd#js-pipeline-triggers
1. Revoke the previous token, which has the description `Upstream trigger token used by gitlab-org/gitlab project`.
1. Create a new token with the same description, and update the `GITLAB_COM_DATABASE_TESTING_TRIGGER_TOKEN` variable at https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd to use it.
1. If all tokens need to be rotated across all projects, also replace the token labeled `Upstream testing trigger (used to self-test the database testing pipeline)`. It corresponds to the variable `TESTING_TRIGGER_TOKEN` in https://gitlab.com/gitlab-org/database-team/gitlab-com-database-testing/-/settings/ci_cd.

### `MERGE_TRAIN_TRIGGER_TOKEN`

This is used to sync any changes pushed to a stable branch to the corresponding
`gitlab-org/gitlab-foss` stable branch.

The merge-train is automatically triggered when a commit is pushed to a stable branch in the `gitlab-org/gitlab` repo.
Take a look at the [`.releases/gitlab-ci.yml`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/releases.gitlab-ci.yml#L4) CI definition
and [`sync-stable-branch.sh`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/sync-stable-branch.sh) script for details.

See [the documentation](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/utilities/merge_train.md).

Ask [a Delivery engineer](https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/#team-members).

### `PACKAGE_HUNTER_USER`, `PACKAGE_HUNTER_PASS`

Ask [a Security Research engineer](https://gitlab.com/groups/gitlab-com/gl-security/security-research/-/wikis/Rotating-the-Package-Hunter-Password).

### `PROJECT_TOKEN_FOR_CI_SCRIPTS_API_USAGE`

1. Create a new `PROJECT_TOKEN_FOR_CI_SCRIPTS_API_USAGE` token with `api` scope, `Developer` and no expiration date at https://gitlab.com/gitlab-org/gitlab/-/settings/access_tokens
1. Update the `PROJECT_TOKEN_FOR_CI_SCRIPTS_API_USAGE` variable at https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd
1. Revoke the previous token

### `QA_1P_GITHUB_UUID`

This is a pointer to an item in 1Password. No need to rotate it.

### `QA_1P_PASSWORD`, `QA_1P_SECRET`

1. Login to https://gitlab.1password.com using the credentials in GitLab-QA 1Password vault for "GitLab-QA 1Password" item
1. Regenerate the secret key and change the password. Download and save the emergency kit file 
1. Update the values in 1Password. The emergency kit file should also be saved in 1Password
1. Update the `ep_infra_e2e_ci_1p_secret`, `ep_infra_e2e_ci_1p_password` variables in Google Secrets Manager (GSM) by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials

For more information, ask [a counterpart SET in the Authentication and Authorization group](https://about.gitlab.com/handbook/product/categories/#authentication-and-authorization-group).

### `QA_ADMIN_ACCESS_TOKEN`

Used in QA tests for API access with admin priviledges.

Current value in GCP is set to the [default development value](https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/fixtures/development/25_api_personal_access_token.rb#L4)
so it doesn't need to be rotated.

### `QA_ALLURE_AWS_ACCESS_KEY_ID`, `QA_ALLURE_AWS_SECRET_ACCESS_KEY`

1. Key is defined in AWS group account `eng-quality-ops-ci-cd-shared-infra-498dbd5a` which can be accessed by:
   * @acunskis
   * @zeffmorgan
   * @treagitlab
   * @svistas
   * @richard.chong
1. Navigate to `IAM` => `Users` in AWS Console
1. Open user `allure-report`
1. Navigate to `Security Credentials`
1. In section `Access keys` click `Create access key`, choose `Other` when prompted for type
1. Update the `ep_infra_e2e_ci_allure_aws_access_key_id`/`ep_infra_e2e_ci_allure_aws_secret_access_key` secrets and corresponding variables by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Remove or set inactive status for previous access key

### `QA_AWS_S3_ACCESS_KEY`, `QA_AWS_S3_KEY_ID`

These are stored in the 1Password `AWS S3 GitLab QA - Scenario: object_storage_aws` item in the QA vault.

1. 1. Key is defined in AWS group account `eng-quality-ops-ci-cd-shared-infra-498dbd5a` which can be accessed by:
   * @acunskis
   * @zeffmorgan
   * @treagitlab
   * @svistas
   * @richard.chong
1. Navigate to `IAM` => `Users` in AWS Console
1. Open user `gitlab-qa`
1. Navigate to `Security Credentials`
1. In section `Access keys` click `Create access key`
1. Update the 1Password item
1. Update the `ep_infra_e2e_ci_aws_s3_access_key`, `ep_infra_e2e_ci_aws_s3_key_id` secrets and corresponding variables by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Ask [a counterpart SET in the Package:Registry group](https://about.gitlab.com/handbook/product/categories/#package-registry-group) to revoke the previous credentials

See [the Handbook page for more information](https://about.gitlab.com/handbook/engineering/development/ops/package/quality/#scenario---run-all-tests-from-the-package-group-against-an-object-storage-provider).

### `QA_EE_ACTIVATION_CODE`, `QA_EE_LICENSE`

These shouldn't need to be rotated, as those values are generated on testing environment using different encryption than production.
It cannot be applied to any production GitLab instance as the values cannot be validated.

For more information, ask [a counterpart SET in the Fulfillment:Provision group](https://about.gitlab.com/handbook/product/categories/#provision-group).

### `QA_GITHUB_ACCESS_TOKEN`

This is a personal token of the `gitlab-qa-github` GitHub user (credentials in 1Password).

1. Log into the `gitlab-qa-github` account (credentials in 1Password)
1. Create a new `GITHUB_ACCESS_TOKEN env var for e2e tests` token with `repo` scope and no expiration date at https://github.com/settings/tokens
1. Update the `ep_infra_e2e_ci_github_access_token` secret and corresponding variable by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Revoke the previous token at https://github.com/settings/tokens

### `QA_GITHUB_OAUTH_APP_ID`

This is an OAuth application ID of the `gitlab-qa` GitHub user (credentials in 1Password). No need to rotate it.

### `QA_GITHUB_OAUTH_APP_SECRET`

This is an OAuth application secret of the `gitlab-qa` GitHub user (credentials in 1Password).

1. Log into the `gitlab-qa` account (credentials in 1Password)
1. Generate a new client secret for the `GitLab-OAuth` application at https://github.com/settings/applications/977173
1. Update the `ep_infra_e2e_ci_github_oauth_app_secret` secret and corresponding variable by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Delete the previous client secret at https://github.com/settings/applications/977173
1. Update the `GITHUB_OAUTH_APP_SECRET` variable at https://gitlab.com/groups/gitlab-org/quality/-/settings/ci_cd

### `QA_GITHUB_PASSWORD`

This is the password of the `gitlab-qa` GitHub user (credentials in 1Password).

1. Log into the `gitlab-qa` account (credentials in 1Password)
1. Update the password at https://github.com/settings/security
1. Update the `ep_infra_e2e_ci_github_password` secret and corresponding variable by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Update the `QA_GITHUB_PASSWORD` variable at https://gitlab.com/groups/gitlab-org/quality/-/settings/ci_cd

### `QA_GITLAB_CI_TOKEN`

This is a `@gitlab-qa` personal access token with `api` scope, used for various CI-related tasks:

- run [the Confiner tool](https://gitlab.com/gitlab-org/quality/confiner)
- retrieve Knapsack reports for end-to-end jobs

1. Log into the `@gitlab-qa` account (credentials in 1Password)
1. Create a new `QA_GITLAB_CI_TOKEN` token with `api` scope and no expiration date at https://gitlab.com/-/profile/personal_access_tokens
1. Update the `ep_infra_e2e_ci_gitlab_ci_token` secret and corresponding variable by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Revoke the previous token at https://gitlab.com/-/profile/personal_access_tokens

### `QA_GITLAB_TLS_CERTIFICATE`

It's a simple test certificate that's used in the Omnibus configuration for testing HTTPS end-to-end scenarios.

This variable is used to [setup a runner in end-to-end context](https://gitlab.com/gitlab-org/gitlab/-/blob/master/qa/qa/service/docker_run/gitlab_runner.rb).

The value of this variable is actually committed in https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/tls_certificates/gitlab/gitlab.test.crt.

### `QA_GCS_BUCKET_NAME`, `QA_GOOGLE_JSON_KEY`, `QA_GOOGLE_PROJECT`, `QA_GOOGLE_CLIENT_EMAIL`

These are stored in the 1Password `GCP Object Storage Scenario` item in the QA vault.

`QA_GCS_BUCKET_NAME`, `QA_GOOGLE_PROJECT`, `QA_GOOGLE_CLIENT_EMAIL` don't need to be rotated.

1. Access the project `group-qa-tests-566cc6` in GCP
1. Go to `IAM` => `Service Accounts` and select the account `object-storage-tests-192`
1. On the `Keys` tab select `Add Key` and then JSON format to obtain the value for `google_json_key`
1. Update the 1Password item
1. Update the `ep_infra_e2e_ci_google_json_key` secret and corresponding variable by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Revoke previous credentials

See [the Handbook page for more information](https://about.gitlab.com/handbook/engineering/development/ops/package/quality/#scenario---run-all-tests-from-the-package-group-against-an-object-storage-provider).

### `QA_GOOGLE_CDN_JSON_KEY`, `QA_GOOGLE_CDN_LB`, `QA_GOOGLE_CDN_SIGNURL_KEY`

These are stored in the 1Password `GCP Registry with CDN scenario` item in the QA vault. Note that `QA_GOOGLE_CDN_JSON_KEY` has the same value as `QA_GOOGLE_JSON_KEY`.

`QA_GOOGLE_CDN_LB` doesn't need to be rotated.

1. Access the project `group-qa-tests-566cc6` in GCP
1. Go to `IAM` => `Service Accounts` and select the account `object-storage-tests-192`
1. On the `Keys` tab select `Add Key` and then JSON format to obtain the value for `google_cdn_json_key`
1. Search for `Cloud CDN` and access the `cdn-registry-tests` bucket
1. Click `Edit` => `Cache performance` and locate the Restricted content section 
1. Click `add signing key` and delete the previous one. If modifying the key name don't forget to alter in the [e2e ci module](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/qa-resources/modules/e2e-ci/main.tf)
1. Update the 1Password item
1. Update the `ep_infra_e2e_ci_google_cdn_json_key`, `ep_infra_e2e_ci_google_cdn_signurl_key` secrets and corresponding variables by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Revoke previous credentials

See [the Handbook page for more information](https://about.gitlab.com/handbook/engineering/development/ops/package/quality/#cdn-scenario---run-the-registry-with-a-google-cdn-enabled).

### `QA_INFLUXDB_TOKEN`

1. Log in to [InfluxDb](https://influxdb.quality.gitlab.net/) instance using `admin` user using credentials from `Quality` vault in `1Password`
1. Navigate to `Load Data` => `API tokens`
1. Click on `Generate API Token` => `Custom API token`
1. Add description `QA token` and under `Buckets` choose `Read` and `Write` for following buckets:
   * `e2e-test-stats-dev`
   * `e2e-test-stats-main`
   * `test-env-stats`
1. After selecting buckets, click `Generate`
1. Update the `ep_infra_e2e_ci_influxdb_token` secret and corresponding variable by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Log in to [QA Grafana](https://dashboards.quality.gitlab.net/) instance using `admin` user using credentials from `Quality` vault in `1Password`
1. Navigate to `Configuration` => `Data sources`
1. Update token for `InfluxDB`, `InfluxDB_Main_Runs` and `InfluxDB_Test_Envs` sources using following procedure:
   1. Click on the data source
   1. Under `Custom HTTP Headers` click `Reset` button
   1. In the value field input `Token` + `space` + new token value. Example: `Token new_token_value`
   1. Click `Save & test` and make sure `data source is working` popup appears
1. Remove old token in `InfluxDb` UI

### `QA_JIRA_ADMIN_PASSWORD`

This shouldn't need to be rotated, as this value is only used to access the Jira server that's started as a container against end-to-end tests are run.

**In the future we should change the credentials to be "dummy", e.g. admin/1234 and commit them in plain text as defaults in qa/qa/runtime/env.rb to simplify the setup.**

For more information, ask [a counterpart SET in the Manage:Integrations group](https://about.gitlab.com/handbook/product/categories/#integrations-group).

### `QA_KNAPSACK_REPORT_GCS_CREDENTIALS`

1. Log in to `google cloud console` and open project `gitlab-qa-resources`
1. Navigate to `IAM & Admin` => `Service Accounts`
1. Open service account `knapsack-report@gitlab-qa-resources.iam.gserviceaccount.com`
1. Navigate to `KEYS` tab, generate new key and save key json file
1. Update the `ep_infra_e2e_ci_knapsack_report_gcs_credentials` secret and corresponding variable by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Remove previous key from `knapsack-report@gitlab-qa-resources.iam.gserviceaccount.com` service account

### `QA_RESULTS_ISSUE_PROJECT_TOKEN`

This is a project token of https://gitlab.com/gitlab-org/quality/testcases. This is used in the `report-results` job to create RSpec run results issues.

1. Create a new `QA_RESULTS_ISSUE_PROJECT_TOKEN` token with `api` scope, `Reporter` role and no expiration date at https://gitlab.com/gitlab-org/quality/testcases/-/settings/access_tokens
1. Update the `ep_infra_e2e_ci_results_issue_project_token` secret and corresponding variable by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Revoke the previous token at https://gitlab.com/gitlab-org/quality/testcases/-/settings/access_tokens

### `QA_TEST_CASE_PROJECT_TOKEN`

This is a project token of https://gitlab.com/gitlab-org/gitlab. This is used in E2E jobs (by the `report-results` and `relate-failure-issue` scripts) to link RSpec run results to test cases, and create/update issues for RSpec failed examples.

1. Create a new `QA_TEST_CASE_PROJECT_TOKEN` token with `api` scope, `Maintainer` role and no expiration date at https://gitlab.com/gitlab-org/gitlab/-/settings/access_tokens
1. Update the `ep_infra_e2e_ci_test_case_project_token` secret and corresponding variable by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Revoke the previous token at https://gitlab.com/gitlab-org/gitlab/-/settings/access_tokens

### `QA_TEST_SESSION_TOKEN`

This is a project token of https://gitlab.com/gitlab-org/quality/testcase-sessions. This is used in the `generate-test-session` job to generate test session issues.

1. Create a new `QA_TEST_SESSION_TOKEN` token with `api` scope, `Reporter` role and no expiration date at https://gitlab.com/gitlab-org/quality/testcase-sessions/-/settings/access_tokens
1. Update the `ep_infra_e2e_ci_test_session_token` secret and corresponding variable by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Revoke the previous token at https://gitlab.com/gitlab-org/quality/testcase-sessions/-/settings/access_tokens

### `QA_THIRD_PARTY_DOCKER_PASSWORD`

This "password" is actually a Deploy token of https://gitlab.com/gitlab-org/quality/third-party-docker-private/-/settings/repository
used to access the private `registry.gitlab.com/gitlab-org/quality/third-party-docker-private` container registry.

This is stored in the 1Password `Quality third party docker deploy token` item in the QA vault.

1. Create a new `QA_THIRD_PARTY_DOCKER_PASSWORD` deploy token with `read_registry` scope, `quality-third-party-docker` username and no expiration date at https://gitlab.com/gitlab-org/quality/third-party-docker-private/-/settings/repository
1. Update the 1Password item
1. Update the `ep_infra_e2e_ci_third_party_docker_password` secret and corresponding variable by following https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/blob/main/README.md#how-do-we-rotate-credentials
1. Revoke the previous deploy token at https://gitlab.com/gitlab-org/quality/third-party-docker-private/-/settings/repository

See [the "Running tests that require special setup" page](https://docs.gitlab.com/ee/development/testing_guide/end_to_end/running_tests_that_require_special_setup.html#jenkins-spec).

### `REVIEW_APPS_GCP_CREDENTIALS`

1. Generate a new key at https://console.cloud.google.com/iam-admin/serviceaccounts/details/107840600499524983569/keys?project=gitlab-review-apps
1. Update the `REVIEW_APPS_GCP_CREDENTIALS` file variable at https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd
1. Delete the old key at https://console.cloud.google.com/iam-admin/serviceaccounts/details/107840600499524983569/keys?project=gitlab-review-apps

### `REVIEW_APPS_ROOT_PASSWORD`

1. Generate a new "GitLab EE Review App (new)" login item in 1Password with username being `root`, and a new random password.
1. Update the `REVIEW_APPS_ROOT_PASSWORD` variable at https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd
1. Update the `shared-gitlab-initial-root-password` secrets and current `root` password for all currently deployed Review Apps (script to be written). Alternatively, purging the review app environment would allow the review app to be re-created with the right root password for test execution.

### `REVIEW_APPS_ROOT_TOKEN`

1. Generate a new random token (e.g. by using 1Password).
1. Update the `REVIEW_APPS_ROOT_TOKEN` variable at https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd

Note: this token is only used to disable sign-ups –as a one-time operation– after deploying Review Apps, so there's no need to keep track of its value anywhere else than in the CI/CD variable itself.

### `REVIEW_APPS_SENTRY_DSN`

1. Generate a new key at https://sentry.gitlab.net/settings/gitlab/projects/gitlab-review-apps/keys/
1. Update the `REVIEW_APPS_SENTRY_DSN` variable at https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd
1. Delete the old key at https://sentry.gitlab.net/settings/gitlab/projects/gitlab-review-apps/keys/

### `RUBY2_SYNC_TOKEN`

The token is used to synchronize the `master` branch to the `ruby2` branch.

1. Create a new `RUBY2_SYNC_TOKEN` token with `write_repository` scope, `Developer` role and no expiration date at https://gitlab.com/gitlab-org/gitlab/-/settings/access_tokens
1. Update the `RUBY2_SYNC_TOKEN` variable at https://gitlab.com/gitlab-org/gitlab/-/settings/ci_cd
1. Revoke the previous token

See [the "Pipelines for the GitLab project" page](https://docs.gitlab.com/ee/development/pipelines/index.html#current-versions-testing).

## Variables rotation for https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure

These are defined in https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/settings/ci_cd and [in Google Secret Manager](https://console.cloud.google.com/security/secret-manager).

### Review-apps

#### CI/CD environment variables

##### `KEY_gitlab_review_apps`

This key stores a JSON key for the `engineering-productivity-infra` GCP Service Account:

* [Generate a new JSON key](https://console.cloud.google.com/iam-admin/serviceaccounts/details/108954611398645479599/keys?project=gitlab-review-apps)
* Use this key in the `KEY_gitlab_review_apps` CI variable
* Ensure the older keys are inactive (this step might be done automatically by GCP)

#### Google Secret Manager secrets

##### `ep_infra_review_apps_gitlab_agent_token`

* Create a new token at https://gitlab.com/gitlab-org/gitlab/-/cluster_agents/review-apps?tab=tokens
* [Rotate the credentials](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure#how-do-we-rotate-credentials)

##### `ep_infra_review_apps_aws_access_key` and `ep_infra_review_apps_aws_secret_key`

* Ask in `#infrastructure-lounge` in Slack to rotate the credentials for the AWS account ID `855262394183`.
  * See [this RUNBOOK entry](review-apps.md#how-to-query-dns-records-managed-by-external-dns) for instructions to retrieve that AWS account it.
* Create a new secret version with the new credentials.
* Disable the older secret versions.
* Redeploy review-apps by triggering a new CI/CD pipeline.

You'll also need to manually change a few variables for cert-manager until https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/-/issues/1 is worked on:

```shell
review-apps

# Backup the older resources
mkdir cert-manager-backup
kubectl get issuer review-apps-route53-dns01-wildcard -o yaml > cert-manager-backup/issuer.yaml
kubectl get secret route53-dns01-credentials-secret -o yaml > cert-manager-backup/route53-secret.yaml

kubectl edit issuers review-apps-route53-dns01-wildcard
# Change the .spec.acme.solvers[0].dns01.route53.accessKeyID to the new credential

# Generate a base64 encoding of the new AWS_SECRET_ACCESS_KEY
echo -n '<AWS_SECRET_ACCESS_KEY>' | base64

kubectl edit secret route53-dns01-credentials-secret
# Change the .data.secret-access-key to the new base64-encoded credential

# Kill the cert-manager pods
kubectl delete pods <cert-manager-pods>

# Verify that the orders and challenges are not pending
kubectl get orders, challenges

# Verify that the certificate was successfully renewed
kubectl describe certificates

# When everything is well, delete the local backup
rm -rf cert-manager-backup
```

### QA resources

#### CI/CD environment variables

##### `KEY_gitlab_qa_resources`

This key stores a JSON key for the `engineering-productivity-infra` GCP Service Account:

* [Generate a new JSON key](https://console.cloud.google.com/iam-admin/serviceaccounts/details/106771519625120551539/keys?project=gitlab-qa-resources)
* Use this key in the `KEY_gitlab_qa_resources` CI variable
* Ensure the older keys are inactive (this step might be done automatically by GCP)

##### `TF_VAR_*`

TODO: Fill those in

#### Google Secret Manager secrets

##### `ep_infra_e2e_ci_*`

Ping people in `#quality` Slack channel, and ask them to assign DRIs for the different secrets.

##### `ep_infra_gitlab_runner_*`

TODO: Fill this section

##### `ep_infra_test_metrics_*`

TODO: Fill this section

##### `ep_infra_triage_ops_gitlab_agent_token`

* Create a new token at https://gitlab.com/gitlab-org/quality/triage-ops/-/cluster_agents/triage-ops-prod?tab=tokens
* [Rotate the credentials](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure#how-do-we-rotate-credentials)

##### `ep_infra_triage_ops_gitlab_webhook_token`

* Create a new token at https://gitlab.com/groups/gitlab-org/-/hooks/971582/edit (need to be group owner for `gitlab-org`)
* [Rotate the credentials](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure#how-do-we-rotate-credentials)

## Variables rotation for https://gitlab.com/gitlab-org/ml-ops/tanuki-stan

### `PRIVATE_TOKEN`

1. Log into the `@gitlab-bot` account (credentials in 1Password)
1. Create a new `tanuki-stan scheduled pipeline` token with `api` scope and no expiration date as a developer at https://gitlab.com/gitlab-org/gitlab/-/settings/access_tokens
1. Update the `PRIVATE_TOKEN` variable at https://gitlab.com/gitlab-org/ml-ops/tanuki-stan/-/pipeline_schedules for `Label gitlab-org/gitlab issues` schedule
1. Revoke the previous token at https://gitlab.com/gitlab-org/gitlab/-/settings/access_tokens
