# RUNBOOK - Tracing a `gitlab-org/gitlab` pipeline

[[_TOC_]]

## Prerequisites

1. Clone [release-tools](https://gitlab.com/gitlab-org/release-tools) locally and `cd release-tools` into it.
1. Install the project dependencies with `bundle install`.

## Trace a pipeline

Run the `trace:pipeline_url` rake task as follows:

```
# Replace `URL-OF-PIPELINE-TO-TRACE` with the URL of the pipeline you want to trace
# Replace `EP-OBSERVABILITY-NAMESPACE-TOKEN` with the actual from 1Password's `EP observability namespace token` item in the Engineering vault
# Replace `ACCESS-TOKEN-WITH-API-SCOPE` with a GitLab.com Personal Token with at least the `read_api` scope

SERVICE_NAME="GitLab pipelines" \
PIPELINE_URL="URL-OF-PIPELINE-TO-TRACE" \
GITLAB_O11Y_URL="https://observe.gitlab.com/v1/traces/16947798" \
GITLAB_O11Y_NAMESPACE_TOKEN="EP-OBSERVABILITY-NAMESPACE-TOKEN" \
RELEASE_BOT_PRODUCTION_TOKEN="ACCESS-TOKEN-WITH-API-SCOPE" \
bundle exec rake "trace:pipeline_url[$PIPELINE_URL,$SERVICE_NAME]"
```

Detailed explanation of the variables used in the rake task:

- `SERVICE_NAME` string will appear in multiple places on the UI as an identifier. For example when searching for traces of a particular service, the dropdown will contain the `SERVICE_NAME` you chose as one of the options. The service name will also appear in the trace itself.
- `PIPELINE_URL` is the pipeline URL that you want to generate traces for.
- `GITLAB_O11Y_URL` is the URL to our Observability instance.
- `GITLAB_O11Y_NAMESPACE_TOKEN` is the token we generated from our Observability instance.
- `RELEASE_BOT_PRODUCTION_TOKEN` is a GitLab.com token (usually PAT) with a minimum of `read_api` scope. You can use your own personal access token when running this on your local machine.

## Visualize a pipeline's trace

1. Visit https://observe.gitlab.com/v1/jaeger/16947798.
1. Select `GitLab pipelines` in the `Service` drop-down.
1. Select any job in the `Operation` drop-down.
1. Select `Last 2 Days` in the `Lookback` drop-down, or make sure that the value is set to a time range that is higher than the creation date of the pipeline.
1. Click `Find Traces`. You should your pipeline URL in the right-side of the page.
1. Click on the row that includes your pipeline URL.

![pipeline-tracing-ui.png](assets/pipeline-tracing-ui.png)
